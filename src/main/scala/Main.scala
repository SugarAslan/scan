import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{
  col,
  date_format,
  length,
  lit,
  rank,
  substring,
  to_timestamp,
  when
}

import java.sql.{DriverManager, Statement}
import java.util

object Main {
  def main(args: Array[String]): Unit = {
    val dbsourse = args(0).toUpperCase
    val dbtarget = args(1).toUpperCase

    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver())

    val userDB = "TEST_OUT_SCAN"
    val passDB = "sl&sw83"
    val urlDB = "jdbc:oracle:thin:@exa13-scan1.vtb24.ru:1521/TEST_TRANS"

    val con = DriverManager.getConnection(urlDB, userDB, passDB)
    val stmt = con.createStatement

    val spark: SparkSession = SparkSession.builder
      .appName("DMScan")
      .enableHiveSupport()
      .getOrCreate()

    //    spark.sparkContext.setLogLevel("WARN")

    val scanWinFunc1 =
      Window
        .partitionBy("DOCUMENT_ID", "INN")
        .orderBy(col("DOCUMENT_DATE").desc)

    val scanWinFunc2 =
      Window
        .partitionBy("DOCUMENT_ID", "DEDUP_CLUSTER_ID")
        .orderBy(
          col("DOCUMENT_DATE").desc,
          col("SCAN_LOADING_DATE").desc,
          col("OPERDATE").desc,
          col("INN").desc
        )

    val scanNews = spark.sql(s"from ${dbsourse.toUpperCase}.fct_scan_news_dcld")

    var scan1 = scanNews
      .withColumn("rank", rank over scanWinFunc1)
      .where(col("DEDUP_CLUSTER_ID").isNull)

    scan1 = scan1
      .where(col("rank") === 1)

    scan1 = scan1
      .withColumn("CONTENT1", col("CONTENT").substr(1, 4000))
      .withColumn("ANNOTATION1", substring(col("ANNOTATION"), 1, 4000))
      .withColumn("FIRST_LINE1", substring(col("FIRST_LINE"), 1, 4000))

    var scan2 = scanNews
      .withColumn("rank", rank over scanWinFunc2)
      .where(col("DEDUP_CLUSTER_ID").isNotNull)
    scan2 = scan2
      .where(col("rank") === 1)
      .withColumn("CONTENT1", col("CONTENT").substr(1, 4000))
      .withColumn("ANNOTATION1", substring(col("ANNOTATION"), 1, 4000))
      .withColumn("FIRST_LINE1", substring(col("FIRST_LINE"), 1, 4000))

    val scanNewsFrame = scan1
      .select(
        col("DOCUMENT_ID"),
        col("DEDUP_CLUSTER_ID"),
//        col("DOCUMENT_DATE"),
//        col("SCAN_LOADING_DATE"),
//        col("OPERDATE"),
        to_timestamp(col("DOCUMENT_DATE"), "yyyy-MM-dd'T'HH:mm:ss")
          .as("DOCUMENT_DATE"),
        to_timestamp(
          date_format(col("SCAN_LOADING_DATE"), "yyyy-MM-dd HH:mm:ss"),
          "yyyy-MM-dd HH:mm:ss"
        ).as("SCAN_LOADING_DATE"),
        to_timestamp(col("OPERDATE"), "yyyyMMdd").as("OPERDATE"),
        col("TITLE"),
        col("AUTHOR"),
        col("SOURCE_ID"),
        col("SOURCE_NAME"),
        col("URL"),
        col("ANNOTATION").as("ANNOTATION"),
        col("FIRST_LINE1") as ("FIRST_LINE"),
        when(col("IS_FULL_TEXT_UNAVAILABLE") === true, "1")
          .otherwise("0")
          .alias("IS_FULL_TEXT_UNAVAILABLE"),
        col("CONTENT1") as ("CONTENT"),
        col("SOURCE_FULL_INFO")
      )
      .union(
        scan2.select(
          col("DOCUMENT_ID"),
          col("DEDUP_CLUSTER_ID"),
//          col("DOCUMENT_DATE"),
//          col("SCAN_LOADING_DATE"),
//          col("OPERDATE"),
          to_timestamp(col("DOCUMENT_DATE"), "yyyy-MM-dd'T'HH:mm:ss")
            .as("DOCUMENT_DATE"),
          to_timestamp(
            date_format(col("SCAN_LOADING_DATE"), "yyyy-MM-dd HH:mm:ss"),
            "yyyy-MM-dd HH:mm:ss"
          ).as("SCAN_LOADING_DATE"),
          to_timestamp(col("OPERDATE"), "yyyyMMdd").as("OPERDATE"),
          col("TITLE"),
          col("AUTHOR"),
          col("SOURCE_ID"),
          col("SOURCE_NAME"),
          col("URL"),
          col("ANNOTATION1").as("ANNOTATION"),
          col("FIRST_LINE1").as("FIRST_LINE"),
          when(col("IS_FULL_TEXT_UNAVAILABLE") === true, lit("1"))
            .otherwise(lit("0"))
            .alias("IS_FULL_TEXT_UNAVAILABLE"),
          col("CONTENT1").as("CONTENT"),
          col("SOURCE_FULL_INFO")
        )
      )

    scanNewsFrame.select(length(col("CONTENT"))).show()

    val scanNewsInn = scan1
      .select(
        col("inn"),
        col("document_id"),
        col("dedup_cluster_id"),
        to_timestamp(col("DOCUMENT_DATE"), "yyyy-MM-dd'T'HH:mm:ss")
          .as("DOCUMENT_DATE"),
        to_timestamp(
          date_format(col("SCAN_LOADING_DATE"), "yyyy-MM-dd HH:mm:ss"),
          "yyyy-MM-dd HH:mm:ss"
        ).as("SCAN_LOADING_DATE"),
        to_timestamp(col("OPERDATE"), "yyyyMMdd").as("OPERDATE")
      )
      .union(
        scan2.select(
          col("inn"),
          col("document_id"),
          col("dedup_cluster_id"),
          to_timestamp(col("DOCUMENT_DATE"), "yyyy-MM-dd'T'HH:mm:ss")
            .as("DOCUMENT_DATE"),
          to_timestamp(
            date_format(col("SCAN_LOADING_DATE"), "yyyy-MM-dd HH:mm:ss"),
            "yyyy-MM-dd HH:mm:ss"
          ).as("SCAN_LOADING_DATE"),
          to_timestamp(col("OPERDATE"), "yyyyMMdd").as("OPERDATE")
        )
      )

    val scanNewsFrameName =
      s"${dbtarget.toUpperCase}.scan_news_oracle"

    val scanNewsInnFrameName =
      s"${dbtarget.toUpperCase}.scan_news_inn_oracle"

    val listTables = new util.ArrayList[String]()

    listTables.add(scanNewsFrameName)
    listTables.add(scanNewsInnFrameName)

    DropTable.drop(spark, dbtarget, listTables)

    scanNewsFrame
      .repartition(1)
      .write
      .mode(SaveMode.Append)
      .saveAsTable(s"${dbtarget.toUpperCase}.scan_news_oracle")

    scanNewsInn
      .repartition(1)
      .write
      .mode(SaveMode.Append)
      .saveAsTable(s"${dbtarget.toUpperCase}.scan_news_inn_oracle")

//    println("SCAN_NEWS")
//    writeToOracle(
//      scanNewsFrame,
//      "SCAN_NEWS",
//      stmt,
//      urlDB,
//      userDB,
//      passDB
//    )
//    println("SCAN_NEWS_INN")
//    writeToOracle(
//      scanNewsInn,
//      "SCAN_NEWS_INN",
//      stmt,
//      urlDB,
//      userDB,
//      passDB
//    )
  }

  def writeToOracle(
      df: DataFrame,
      dbTable: String,
      stmt: Statement,
      urlDB: String,
      userDB: String,
      passDB: String
  ): Unit = {
    stmt.execute(s"BEGIN TEST_SCAN.TRUNCATE_TBL('$dbTable'); END;")

    df.show(1)

    df.distinct()
      .write
      .format("jdbc")
      .option("driver", "oracle.jdbc.driver.OracleDriver")
      .option("batchsize", "5000")
      .option("url", urlDB)
      .option("dbtable", "TEST_SCAN." + dbTable)
      .option("user", userDB)
      .option("password", passDB)
      .mode(SaveMode.Append)
      .save()

    println(dbTable + " - OK")
  }
}
