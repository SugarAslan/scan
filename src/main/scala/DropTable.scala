import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

import java.util

object DropTable {
  def drop(
      spark: SparkSession,
      dbTarget: String,
      tables: util.ArrayList[String]
  ) {

    val path = spark
      .sql(s"describe database ${dbTarget}")
      .filter(col("database_description_item") === "Location")
      .select("database_description_value")
      .first()
      .get(0)

    var i = 0
    while (i < tables.size()) {
      val conf = spark.sparkContext.hadoopConfiguration
      val fs = org.apache.hadoop.fs.FileSystem.get(conf)
      val filePath = new org.apache.hadoop.fs.Path(s"${path}/${tables.get(i)}")
      val fullNameTable = s"${tables.get(i).toUpperCase}"
      if (fs.exists(filePath)) fs.delete(filePath, true)
      if (spark.catalog.tableExists(s"${fullNameTable}")) {
        println(s"DROP TABLE IF EXISTS ${fullNameTable}")
        spark.sql(s"DROP TABLE IF EXISTS ${fullNameTable} ")
      } else {
        println(s"NOT SUCH TABLE ${fullNameTable}")
      }
      i += 1
    }
  }
}
